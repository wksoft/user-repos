#cinkciarz.pl interview task

###Building the application
    mvn clean install

###Running the application
    java -jar user-repos-1.0.jar

###Overriding default users/passwords list
    java -jar user-repos-1.0.jar --credentials=newuser:newpasswd

For ease of use passwords should be in plain text instead of BCrypt encrypted.

###Invoking REST service
Endpoint is available under given url:

    localhost:8080/user/{username}/repos
    
Basic authentication is needed. Default username/passwords are:

    user:passwd,test:test