FROM openjdk:10-jre-slim
COPY ./target/user-repos-1.0.jar /usr/src/hola/
WORKDIR /usr/src/hola
EXPOSE 8080
CMD ["java", "-jar", "user-repos-1.0.jar"]