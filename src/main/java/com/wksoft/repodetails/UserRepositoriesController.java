package com.wksoft.repodetails;


import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.client.HttpClientErrorException;
import org.springframework.web.client.ResourceAccessException;

import java.net.SocketTimeoutException;

import java.util.List;

@RestController
public class UserRepositoriesController {

    private UserRepositoriesService userRepositoriesService;

    @Autowired
    public UserRepositoriesController(UserRepositoriesService userRepositoriesService) {
        this.userRepositoriesService = userRepositoriesService;
    }

    @RequestMapping("/user/{owner}/repos")
    public List<Repository> getRepoDetails(
            @PathVariable(value = "owner") String owner,
            @RequestParam(value = "up-to-date", required = false) Boolean isUpToDate,
            @RequestParam(value = "sort-by-name", required = false) String sortByName) {
        return userRepositoriesService.getUserRepositories(owner, isUpToDate, sortByName);
    }

    @ExceptionHandler({ResourceAccessException.class})
    public ResponseEntity<ErrorResponse> handleResourceAccessException(Exception e) {
        if (e.getCause() instanceof SocketTimeoutException) {
            return new ResponseEntity<>(
                    new ErrorResponse("GitLab API access timeout"),
                    HttpStatus.GATEWAY_TIMEOUT);
        }
        return new ResponseEntity<>(
                new ErrorResponse("Can not access the GitLab API"),
                HttpStatus.SERVICE_UNAVAILABLE);
    }

    @ExceptionHandler({HttpClientErrorException.class})
    public ResponseEntity<ErrorResponse> handleHttpClientErrorException(HttpClientErrorException e) {
        if (e.getStatusCode().equals(HttpStatus.NOT_FOUND)) {
            return new ResponseEntity<>(
                    new ErrorResponse("Resource not found"),
                    HttpStatus.NOT_FOUND);
        }
        return new ResponseEntity<>(
                new ErrorResponse("HTTP client error"),
                HttpStatus.BAD_REQUEST);
    }

    @ExceptionHandler({Exception.class})
    public ResponseEntity<ErrorResponse> handleException(Exception e) {
        return new ResponseEntity<>(
                new ErrorResponse("Internal server error"),
                HttpStatus.INTERNAL_SERVER_ERROR);
    }


}
