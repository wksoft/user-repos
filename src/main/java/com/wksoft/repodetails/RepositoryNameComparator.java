package com.wksoft.repodetails;

import org.springframework.stereotype.Component;

import java.util.Comparator;

@Component
public class RepositoryNameComparator implements Comparator<Repository>{

    @Override
    public int compare(Repository o1, Repository o2) {
        return o1.getName().compareTo(o2.getName());
    }

}
