package com.wksoft.repodetails;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.ArrayList;
import java.util.List;

@Component
public class RepositoryMapper {

    private GitHubRepositoryUpToDatePredicate upToDatePredicate;

    @Autowired
    public RepositoryMapper(GitHubRepositoryUpToDatePredicate upToDatePredicate) {
        this.upToDatePredicate = upToDatePredicate;
    }

    public List<Repository> map(GitHubRepository[] repositories) {
        List<Repository> result = new ArrayList<>();
        for (GitHubRepository repository : repositories) {
            result.add(new Repository(
                    repository.getName(),
                    repository.getLanguage(),
                    repository.getCreationDate(),
                    repository.getUpdateDate(),
                    isUpToDate(repository)
            ));
        }
        return result;
    }

    private boolean isUpToDate(GitHubRepository repository) {
        return upToDatePredicate.test(repository);
    }
}
