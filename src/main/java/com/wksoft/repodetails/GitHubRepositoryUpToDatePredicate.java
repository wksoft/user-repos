package com.wksoft.repodetails;

import org.springframework.stereotype.Component;

import java.time.ZonedDateTime;
import java.util.function.Predicate;

@Component
public class GitHubRepositoryUpToDatePredicate implements Predicate<GitHubRepository>{

    @Override
    public boolean test(GitHubRepository repository) {
        ZonedDateTime updateDate = repository.getUpdateDate();
        if (updateDate == null) {
            return false;
        }
        return !updateDate.isBefore(ZonedDateTime.now().minusDays(90));
    }
}
