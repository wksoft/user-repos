package com.wksoft.repodetails;

import java.time.ZonedDateTime;
import java.util.Date;

public class Repository {

    private final String name;
    private final String language;
    private final ZonedDateTime creationDate;
    private final ZonedDateTime updateDate;
    private final boolean isUpToDate;


    public Repository(String name, String language, ZonedDateTime creationDate, ZonedDateTime updateDate, boolean isUpToDate) {
        this.name = name;
        this.language = language;
        this.creationDate = creationDate;
        this.updateDate = updateDate;
        this.isUpToDate = isUpToDate;
    }

    public String getName() {
        return name;
    }

    public String getLanguage() {
        return language;
    }

    public ZonedDateTime getCreationDate() {
        return creationDate;
    }

    public ZonedDateTime getUpdateDate() {
        return updateDate;
    }

    public boolean isUpToDate() {
        return isUpToDate;
    }
}
