package com.wksoft.repodetails;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.security.config.annotation.authentication.builders.AuthenticationManagerBuilder;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.security.web.AuthenticationEntryPoint;

import java.util.List;

@Configuration
@EnableWebSecurity
public class SecurityConfiguration extends WebSecurityConfigurerAdapter {

    private AuthenticationEntryPoint authEntryPoint;

    private List<String> credentials;

    @Autowired
    public SecurityConfiguration(AuthenticationEntryPoint authEntryPoint,
                                 @Value("#{'${credentials}'.split(',')}") List<String> credentials) {
        this.authEntryPoint = authEntryPoint;
        this.credentials = credentials;
    }

    @Override
    protected void configure(HttpSecurity http) throws Exception {
        http.csrf().disable().authorizeRequests()
                .anyRequest().authenticated()
                .and().httpBasic()
                .authenticationEntryPoint(authEntryPoint);
    }

    @Autowired
    public void configureGlobal(AuthenticationManagerBuilder auth) throws Exception {
        for (String credential : credentials) {
            String[] parsed = credential.split(":");
            String username = parsed[0];
            String plainPassword = parsed[1];
            auth.inMemoryAuthentication()
                    .withUser(username)
                    .password(encoder().encode(plainPassword))
                    .roles("USER");
        }
    }

    @Bean
    public BCryptPasswordEncoder encoder() {
        return new BCryptPasswordEncoder();
    }

}