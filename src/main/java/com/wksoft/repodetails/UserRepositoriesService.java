package com.wksoft.repodetails;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;
import org.springframework.web.util.UriComponentsBuilder;

import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.Stream;

@Service
public class UserRepositoriesService {

    private RestTemplate restTemplate;

    private RepositoryMapper mapper;

    private RepositoryNameComparator nameComparator;

    private String baseUrl;

    private static final Logger log = LoggerFactory.getLogger(UserRepositoriesService.class);

    @Autowired
    public UserRepositoriesService(RestTemplate restTemplate,
                                   RepositoryMapper mapper,
                                   RepositoryNameComparator nameComparator,
                                   @Value("${baseUrl}") String baseUrl) {
        this.restTemplate = restTemplate;
        this.mapper = mapper;
        this.nameComparator = nameComparator;
        this.baseUrl = baseUrl;
    }

    public List<Repository> getUserRepositories(String owner, Boolean isUpToDate, String sortByName) {
        String url = createUrl(owner);
        log.info("Trying to fetch repositories for {}...", owner);
        GitHubRepository[] gitHubRepositories = restTemplate.getForObject(url, GitHubRepository[].class);
        log.info("Fetching repositories succeed: {}", gitHubRepositories.toString());
        List<Repository> repositories = mapper.map(gitHubRepositories);

        Stream<Repository> stream = repositories.stream();
        if (Boolean.TRUE.equals(isUpToDate)) {
            stream = stream.filter(repository -> repository.isUpToDate());
        } else if (Boolean.FALSE.equals(isUpToDate)) {
            stream = stream.filter(repository -> !repository.isUpToDate());
        }
        if ("asc".equals(sortByName)) {
            stream = stream.sorted(nameComparator);
        } else if ("desc".equals(sortByName)) {
            stream = stream.sorted(nameComparator.reversed());
        }
        return stream.collect(Collectors.toList());
    }

    private String createUrl(String owner) {
        UriComponentsBuilder builder = UriComponentsBuilder
                .fromHttpUrl(baseUrl)
                .path("/{owner}")
                .path("/repos");
        return builder
                .buildAndExpand(owner)
                .toUriString();
    }
}
