package com.wksoft.repodetails;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.xebialabs.restito.semantics.Action;
import com.xebialabs.restito.semantics.Condition;
import com.xebialabs.restito.server.StubServer;
import org.glassfish.grizzly.http.util.HttpStatus;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.web.client.RestTemplate;
import org.springframework.web.context.WebApplicationContext;

import java.time.ZonedDateTime;
import java.util.Arrays;
import java.util.List;

import static com.xebialabs.restito.builder.stub.StubHttp.whenHttp;
import static org.hamcrest.Matchers.contains;
import static org.hamcrest.Matchers.hasSize;
import static org.hamcrest.Matchers.is;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.print;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@RunWith(SpringRunner.class)
@SpringBootTest(properties = "baseUrl=http://localhost:8888/users")
public class UserReposMockedGitlabApiApplicationTests {

    @Autowired
    private WebApplicationContext wac;

    @Autowired
    RestTemplate restTemplate;

    @Autowired
    ObjectMapper mapper;

    private MockMvc mockMvc;

    private StubServer server;

    @Before
    public void setup() throws Exception {
        mockMvc = MockMvcBuilders.webAppContextSetup(this.wac).build();
        server = new StubServer(8888).run();
        initializeMockResponse();
    }

    private void initializeMockResponse() throws JsonProcessingException {
        List<GitHubRepository> details = prepareRepoDetails();
        String content = mapper.writeValueAsString(details);
        whenHttp(server).
                match(Condition.get("/users/test/repos")).
                then(Action.status(HttpStatus.OK_200),
                        Action.contentType("application/json;charset=UTF-8"),
                        Action.stringContent(content));
    }

    private List<GitHubRepository> prepareRepoDetails() {
        return Arrays.asList(
                details("K project", "Java", "2007-12-03T10:15:30Z", "2007-12-03T10:15:30Z"),
                details("A project", "PHP", "2007-12-03T10:15:30Z", ZonedDateTime.now().toString()),
                details("Z project", "C#", "2007-12-03T10:15:30Z", ZonedDateTime.now().toString())
        );
    }

    private GitHubRepository details(String name, String language, String creationDate, String updateDate) {
        GitHubRepository details = new GitHubRepository();
        details.setName(name);
        details.setLanguage(language);
        details.setCreationDate(ZonedDateTime.parse(creationDate));
        details.setUpdateDate(ZonedDateTime.parse(updateDate));
        return details;
    }

    @Test
    public void shouldReceiveResponse() throws Exception {

        mockMvc.perform(get("/user/test/repos"))
                .andDo(print())
                .andExpect(status().isOk())
                .andExpect(jsonPath("$").isArray())
                .andExpect(jsonPath("$", hasSize(3)))
                .andReturn();
    }

    @Test
    public void shouldReceiveMappedFields() throws Exception {
        mockMvc.perform(get("/user/test/repos"))
                .andDo(print())
                .andExpect(jsonPath("$[0].name", is("K project")))
                .andExpect(jsonPath("$[0].language", is("Java")))
                .andExpect(jsonPath("$[0].creationDate", is("2007-12-03T10:15:30Z")))
                .andExpect(jsonPath("$[0].updateDate", is("2007-12-03T10:15:30Z")))
                .andExpect(jsonPath("$[0].upToDate", is(false)))
                .andReturn();
    }

    @Test
    public void shouldFilterUpToDateRepos() throws Exception {
        mockMvc.perform(get("/user/test/repos?up-to-date=true"))
                .andDo(print())
                .andExpect(jsonPath("$[*].upToDate", contains(true, true)))
                .andReturn();
    }

    @Test
    public void shouldFilterNotUpToDateRepos() throws Exception {
        mockMvc.perform(get("/user/test/repos?up-to-date=false"))
                .andDo(print())
                .andExpect(jsonPath("$[*].upToDate", contains(false)))
                .andReturn();
    }

    @Test
    public void shouldSortByNameDescending() throws Exception {
        mockMvc.perform(get("/user/test/repos?sort-by-name=desc"))
                .andDo(print()).andExpect(jsonPath("$[0].name", is("Z project")))
                .andDo(print()).andExpect(jsonPath("$[1].name", is("K project")))
                .andDo(print()).andExpect(jsonPath("$[2].name", is("A project")))
                .andReturn();
    }

    @Test
    public void shouldSortByNameAscending() throws Exception {
        mockMvc.perform(get("/user/test/repos?sort-by-name=asc"))
                .andDo(print()).andExpect(jsonPath("$[0].name", is("A project")))
                .andDo(print()).andExpect(jsonPath("$[1].name", is("K project")))
                .andDo(print()).andExpect(jsonPath("$[2].name", is("Z project")))
                .andReturn();
    }

    @Test
    public void shouldFilterUpToDateAndSortByNameDescending() throws Exception {
        mockMvc.perform(get("/user/test/repos?up-to-date=true&sort-by-name=desc"))
                .andDo(print()).andExpect(jsonPath("$[0].name", is("Z project")))
                .andDo(print()).andExpect(jsonPath("$[1].name", is("A project")))
                .andReturn();
    }

    @Test
    public void shouldReturn404ForBadUrl() throws Exception {
        mockMvc.perform(get("/xxx/y/z"))
                .andDo(print()).andExpect(status().isNotFound())
                .andReturn();
    }

    @Test
    public void shouldHandle500Status() throws Exception {
        whenHttp(server).
                match(Condition.get("/users/error/repos")).
                then(Action.status(HttpStatus.INTERNAL_SERVER_ERROR_500));

        mockMvc.perform(get("/user/error/repos"))
                .andDo(print())
                .andExpect(status().is5xxServerError())
                .andExpect(jsonPath("$.errorMessage").value("Internal server error"))
                .andReturn();
    }

    @Test
    public void shouldHandleTimeout() throws Exception {
        whenHttp(server).
                match(Condition.get("/users/timeout/repos")).
                then(Action.delay(10000), Action.stringContent("{}"));

        mockMvc.perform(get("/user/timeout/repos"))
                .andDo(print())
                .andExpect(status().is(504))
                .andExpect(jsonPath("$.errorMessage").value("GitLab API access timeout"))
                .andReturn();
    }

    @After
    public void tearDown() throws Exception {
        server.stop();
    }
}
